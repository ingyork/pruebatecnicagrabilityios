//
//  SubcategoryViewController.swift
//  GrabilityTest
//
//  Created by Jorge Castro on 06/13/18.
//  Copyright © 2018 Jorge Castro. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import Kingfisher
import RxSwift

class MovieViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    let reuseIdentifier = "cell"
    var films = [FilmDTO]()
    var subscription = [Disposable]()
    var categoryId: Int? = nil
    var categoryRawId: String? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let btn = UIBarButtonItem(image: #imageLiteral(resourceName: "search_item_menu"), style: .plain, target: self, action: #selector(btnSearch))
        self.navigationItem.rightBarButtonItem = btn
        
        self.navigationItem.title = "MOVIES"
        self.collectionView?.backgroundColor = .black
        //self.view.showBlurLoader()
        
        initSubscription()
        
        if let categoryRawId = categoryRawId, let categoryId = categoryId {
            MoviewViewModel.dispatchListMoviesBy(categoryId: categoryId, category: categoryRawId)
        }
        
        // Register cell classes
        self.collectionView!.register(UINib(nibName: "MovieCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: reuseIdentifier)
    }
    
    @objc func btnSearch(){
        let controller = SearchViewController()
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        appStore.dispatch(NavigationAction.showNavigationBar)
    }

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let padding: CGFloat =  0
        let collectionViewSize = collectionView.frame.size.width - padding
        return CGSize(width: (collectionViewSize / 2), height: (collectionViewSize / 1.5))
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (films != nil) ? films.count : 0
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.collectionView?.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! MovieCollectionViewCell
        
        let film = films[indexPath.row]
        if let movieId = film.id {
            cell.movieId = movieId
        }
        cell.titleMovie.text = film.title
        
        let urlImg = ApiServices.BASE_URL_IMAGE + film.posterPath!
        let url = URL(string: urlImg)
        
        cell.imageMovie.kf.indicatorType = .activity
        let placeholder = UIImage(named: "img_placeholder_item")
        cell.imageMovie.kf.setImage(with: url, placeholder: placeholder, options: nil, progressBlock: nil, completionHandler: nil)
        
        
        let realVoteAverage = film.voteAverage! / 2
        cell.ratingBar.rating = realVoteAverage
        cell.ratingBar.text = String(describing: realVoteAverage)
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        navigateToDetail(filmSelected: films[indexPath.row])
    }
    
}


extension MovieViewController {
    
    func initSubscription(){
        subscription.append(MoviewViewModel.movieSubject.subscribe(onNext: { films in
            
            if let films = films {
                self.films = films
                self.collectionView?.reloadData()
            }
            //self.view.removeBluerLoader()
        }))
    }
    
    func navigateToDetail(filmSelected: FilmDTO){
        
        appStore.dispatch(DetailAction.UpdateFilmDetail(filmDetail: filmSelected))
        let controller = DetailViewController()
        self.navigationController?.pushViewController(controller, animated: true)
    }
}
