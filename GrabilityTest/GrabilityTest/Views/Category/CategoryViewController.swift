//
//  ViewController.swift
//  GrabilityTest
//
//  Created by Jorge Castro on 6/10/18.
//  Copyright © 2018 Jorge Castro. All rights reserved.
//

import UIKit
import RealmSwift
import RxSwift
import ReSwift

class CategoryViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout, CategoryCallback {
    
    let cellIdentifier = "cellIdentifier"
    var listCategories: [CategoryEntity] = []
    var subscription = [Disposable]()
    let dbHelper = DBHelper()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.navigationItem.title = "CATEGORIES"
        createCategories()
        collectionView?.backgroundColor = UIColor.lightGray
        collectionView?.register(CategoryCollectionViewCell.self, forCellWithReuseIdentifier: cellIdentifier)
        
        appDelegateObservable.subscribe(onNext: { (state) in
            switch state {
            case .willEnterForeground:
                self.initSubscription()
            case .willEnterBackground:
                RxUtils.unsubscribe(subscription: self.subscription)
            default:
                break
            }
        })
        
    }
    
    func createCategories(){
        let popular = CategoryEntity()
        popular.id = 1
        popular.rawId = "popular"
        popular.name = "Popular"
        popular.imageName = "popular_movie.jpg"
        
        let toprated = CategoryEntity()
        toprated.id = 2
        toprated.rawId = "top_rated"
        toprated.name = "Top Rated"
        toprated.imageName = "toprated_movie.jpg"
        
        let upcoming = CategoryEntity()
        upcoming.id = 3
        upcoming.rawId = "upcoming"
        upcoming.name = "Upcoming"
        upcoming.imageName = "upcoming_movie.jpg"
        
        listCategories.append(popular)
        listCategories.append(toprated)
        listCategories.append(upcoming)
        
        dbHelper.insertRows(listCategories)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        appStore.dispatch(NavigationAction.hideNavigationBar)
        initSubscription()
        GenreViewModel.dispatchGenre()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        RxUtils.unsubscribe(subscription: self.subscription)
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return listCategories.count
    }
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! CategoryCollectionViewCell
        
        let category = listCategories[indexPath.row]
        cell.titleItem.text = category.name
        cell.imageItem.image = UIImage(named: category.imageName!)
        cell.categoryId = category.id
        cell.categoryRawId = category.rawId
        cell.callback = self
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: 300)
    }
    
    func navigateToMovies( categoryId: Int, categoryRawId: String) {
        
        let flowLayout = UICollectionViewFlowLayout()
        let controller = MovieViewController(collectionViewLayout: flowLayout)
        controller.categoryId = categoryId
        controller.categoryRawId = categoryRawId
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
}

extension CategoryViewController {
    func initSubscription(){
        subscription.append(GenreViewModel.subject.subscribe(onNext: { (categories) in
            if let categories = categories {
                
                var catEntities = [GenreEntity]()
                categories.forEach({ (category) in
                    let catEntity = GenreEntity()
                    
                    if let id = category.id, let name = category.name {
                        catEntity.id = id
                        catEntity.name = name
                        catEntities.append(catEntity)
                    }
                    
                })
                
                self.dbHelper.insertRows(catEntities)
            }
        }))
    }
}
