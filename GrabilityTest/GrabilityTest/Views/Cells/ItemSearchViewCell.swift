//
//  ItemSearchViewCell.swift
//  GrabilityTest
//
//  Created by Jorge Castro on 6/27/18.
//  Copyright © 2018 Jorge Castro. All rights reserved.
//

import UIKit

class ItemSearchViewCell: UICollectionViewCell {

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var image: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
