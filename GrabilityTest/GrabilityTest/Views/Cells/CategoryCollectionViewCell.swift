//
//  SimpleCollectionViewCell.swift
//  GrabilityTest
//
//  Created by Jorge Castro on 6/11/18.
//  Copyright © 2018 Jorge Castro. All rights reserved.
//

import UIKit

class CategoryCollectionViewCell: UICollectionViewCell {
    
    var callback: CategoryCallback?
    var categoryId: Int?
    var categoryRawId: String?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addGestureRecognizer(
            UITapGestureRecognizer(target: self, action: #selector(self.checkAction(sender:)))
        )
        setupViews()
    }
    
    @objc func checkAction(sender : UITapGestureRecognizer) {
        if let categoryId = categoryId, let categoryRawId = categoryRawId{
            callback?.navigateToMovies(categoryId: categoryId, categoryRawId: categoryRawId)
        }
    }
    
    let imageItem: UIImageView = {
        var imageView : UIImageView
        imageView  = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.image = UIImage(named:"pizzas_clasicas_preview.png")
        return imageView
    }()
    
    let titleItem: UILabel = {
        let label = UILabel()
        label.text = "Title of Item"
        label.backgroundColor = UIColor(red: 0.15, green: 0.15, blue: 0.15, alpha: 0.85)
        label.textAlignment = NSTextAlignment.center
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: 18.0)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    func setupViews(){
        
        backgroundColor = UIColor.green
        addImageInView()
        addTilteInView()
    }
    
    func addTilteInView(){
        addSubview(titleItem)
        addConstraints(
            [
                NSLayoutConstraint(item: titleItem, attribute: .bottom, relatedBy: .equal, toItem: imageItem, attribute: .bottom, multiplier: 1, constant: 0),
                
                NSLayoutConstraint(item: titleItem, attribute: .width, relatedBy: .equal, toItem: self, attribute: .width, multiplier: 1, constant: 0),
                
                NSLayoutConstraint(item: titleItem, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 60)
                
            ]
        )
    }
    
    func addImageInView(){
        addSubview(imageItem)
        addConstraints(
        [
            NSLayoutConstraint(item: imageItem,
                attribute: .leading,
                relatedBy: .equal,
                toItem: self,
                attribute: .leadingMargin,
                multiplier: 0.001,
                constant: 0),
    
            NSLayoutConstraint(item: imageItem,
                attribute: .trailing,
                relatedBy: .equal,
                toItem: self,
                attribute: .trailingMargin,
                multiplier: 1.1,
                constant: 0),
            
            NSLayoutConstraint(item: imageItem,
                attribute: .top,
                relatedBy: .equal,
                toItem: self,
                attribute: .top,
                multiplier: 1,
                constant: 0),
            
            NSLayoutConstraint(item: imageItem,
                attribute: .bottom,
                relatedBy: .equal,
                toItem: self,
                attribute: .bottom,
                multiplier: 1,
                constant: 0)
        ]
    )
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

protocol CategoryCallback {
    func navigateToMovies(categoryId: Int, categoryRawId: String)
}
