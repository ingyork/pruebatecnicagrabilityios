//
//  MovieCollectionViewCell.swift
//  GrabilityTest
//
//  Created by Jorge Castro on 06/13/18.
//  Copyright © 2018 Jorge Castro. All rights reserved.
//

import UIKit
import Cosmos

class MovieCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imageMovie: UIImageView!
    @IBOutlet weak var titleMovie: UILabel!
    @IBOutlet weak var ratingBar: CosmosView!
    
    var movieId: Int64 = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
