//
//  DetailViewController.swift
//  GrabilityTest
//
//  Created by Jorge Castro on 6/22/18.
//  Copyright © 2018 Jorge Castro. All rights reserved.
//

import UIKit
import Kingfisher
import RxSwift
import Cosmos

class DetailViewController: UIViewController {

    @IBOutlet weak var posterFilm: UIImageView!
    @IBOutlet weak var voteCount: UILabel!
    @IBOutlet weak var labelVoteAverage: UILabel!
    @IBOutlet weak var ratingBar: CosmosView!
    @IBOutlet weak var titleMovie: UILabel!
    @IBOutlet weak var urlMovie: UILabel!
    @IBOutlet weak var overview: UILabel!
    @IBOutlet weak var genres: UILabel!
    @IBOutlet weak var productCompanies: UILabel!
    
    var subscription: [Disposable] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initSubscription()
        settingsControls()
        
        if let filmDetail = appStore.state.detailState.film {
            self.navigationItem.title = filmDetail.title
            if let movieId = filmDetail.id {
                DetailViewModel.dispatchDetailMovie(movieId: movieId)
            }
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        appStore.dispatch(NavigationAction.showNavigationBar)
    }
    
    private func showMovieDetail(_ movie: FilmDTO){
        
        if let pathImg = movie.backdropPath {
            let url = URL(string: ApiServices.BASE_URL_IMAGE + pathImg)
            let placeholder = UIImage(named: "img_placeholder_item")
            posterFilm.kf.indicatorType = .activity
            posterFilm.kf.setImage(with: url, placeholder: placeholder, options: nil, progressBlock: nil, completionHandler: nil)
        }
        
        voteCount.text = String(describing: movie.voteCount!)
        if var voteAverage = movie.voteAverage {
            voteAverage = voteAverage / 2
            labelVoteAverage.text = String(describing: voteAverage)
            ratingBar.rating = voteAverage;
        }
        ratingBar.rating = (movie.voteAverage != nil) ? (movie.voteAverage! / 2) : 0.0
        titleMovie.text = movie.title
        let url = (movie.homePage != nil) ? movie.homePage! : ""
        urlMovie.attributedText = NSAttributedString(string: url, attributes: [.underlineStyle: NSUnderlineStyle.styleSingle.rawValue])
        overview.text = movie.overview
        var genresString = ""
        movie.genres.forEach({ genresString += "- \($0.name!)\n" })
        genres.text = genresString
        
        var productionCompaniesString = ""
        movie.productionCompanies.forEach { company in
            productionCompaniesString += "- \(company.name!)\n"
        }
        productCompanies.text = productionCompaniesString
    }
}


extension DetailViewController {
    
    func initSubscription(){
        subscription.append(DetailViewModel.subject.subscribe(onNext: { film in
            if let film = film {
                self.showMovieDetail(film)
            }
        }))
    }
    
    func settingsControls(){
        ratingBar.settings.updateOnTouch = false
        ratingBar.isUserInteractionEnabled = false
        ratingBar.settings.fillMode = .precise
    }
}
