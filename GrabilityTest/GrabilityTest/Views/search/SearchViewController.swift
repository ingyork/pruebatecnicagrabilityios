//
//  SearchViewController.swift
//  GrabilityTest
//
//  Created by Jorge Castro on 6/26/18.
//  Copyright © 2018 Jorge Castro. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController, UISearchBarDelegate, UITextFieldDelegate {

    @IBOutlet weak var headerContainer: UIView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var btnBackNavigation: UIImageView!
    
    var collectionView: SearchCollectionViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(SearchViewController.backNavigation))
        btnBackNavigation.isUserInteractionEnabled = true
        btnBackNavigation.addGestureRecognizer(singleTap)
        
        searchBar.delegate = self
        searchBar.returnKeyType = .done
        
        
        collectionView = SearchCollectionViewController(collectionViewLayout: UICollectionViewFlowLayout())
        addChildViewController(collectionView!)
        if let collectionView = collectionView {
            collectionView.view.frame = containerView.bounds
            containerView.addSubview(collectionView.view)
            collectionView.didMove(toParentViewController: self)
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        appStore.dispatch(NavigationAction.hideNavigationBar)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func backNavigation() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
    
        if let pattern = searchBar.text {
            GenreViewModel.dispatchGroupFilms(patter: pattern)
            searchBar.resignFirstResponder()
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
}
