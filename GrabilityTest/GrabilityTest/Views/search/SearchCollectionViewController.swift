//
//  SearchCollectionViewController.swift
//  GrabilityTest
//
//  Created by Jorge Castro on 6/27/18.
//  Copyright © 2018 Jorge Castro. All rights reserved.
//

import UIKit
import RxSwift
import Kingfisher

private let reuseIdentifier = "Cell"
private let reuseIdentifierHeader = "headerViewcell"

class SearchCollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    var subscriptions = [Disposable]()
    var objectArray = [Objects]()
    var groupsFilms = [GroupFilmGenreDTO]()
    
    struct Objects {
        var sectionName: String!
        var sectionObjects: [String]!
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        let backgroundImage : UIImageView = {
            let iv = UIImageView()
            iv.image = UIImage(named: "background_launch")
            iv.contentMode = .scaleAspectFill
            return iv
        }()
        
        backgroundImage.addBlurEffect()
        self.collectionView?.backgroundView = backgroundImage
        self.collectionView!.register(UINib(nibName: "ItemSearchViewCell", bundle: nil), forCellWithReuseIdentifier: reuseIdentifier)
        
        self.collectionView?.register(UINib(nibName: "HeaderSearchReusableView", bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionHeader , withReuseIdentifier: reuseIdentifierHeader)
 
        objectArray = [
            Objects(sectionName: "Section 1", sectionObjects: ["Section 1.1", "Section 1.2", "Section 1.3", "Section 1.4", "Section 1.5"]),
            Objects(sectionName: "Section 2", sectionObjects: ["Section 2.1", "Section 2.2", "Section 2.3", "Section 2.4", "Section 2.5"]),
            Objects(sectionName: "Section 3", sectionObjects: ["Section 3.1", "Section 3.2", "Section 3.3", "Section 3.4", "Section 3.5"])
        ]
        
        initSubscription()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }


    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return groupsFilms.count
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return groupsFilms[section].films.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! ItemSearchViewCell
    
        // Configure the cell
        let film = groupsFilms[indexPath.section].films[indexPath.row]
        cell.title.text = film.title
        
        let urlImg = ApiServices.BASE_URL_IMAGE + ((film.posterPath != nil) ? film.posterPath! : "")
        let url = URL(string: urlImg)
        cell.image.kf.indicatorType = .activity
        let placeholder = UIImage(named: "img_placeholder_item")
        cell.image.kf.setImage(with: url, placeholder: placeholder, options: nil, progressBlock: nil, completionHandler: nil)
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: 100, height: 70)
    }

    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: 80)
    }
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind:    String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        switch kind {
        case UICollectionElementKindSectionHeader:
            
            let reusableview = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: reuseIdentifierHeader, for: indexPath) as! HeaderSearchReusableView
            
            
            reusableview.sectionTitle.text = groupsFilms[indexPath.section].groupName
            return reusableview
            
            
        default:  fatalError("Unexpected element kind")
        }
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: self.view.frame.width, height: 120)  // Header size
    }
    
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        let frame : CGRect = self.view.frame
        let margin  = (frame.width - 90 * 3) / 6.0
        return UIEdgeInsetsMake(10, margin, 10, margin) // margin between cells
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.5
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let filmSelected = groupsFilms[indexPath.section].films[indexPath.row]
        appStore.dispatch(DetailAction.UpdateFilmDetail(filmDetail: filmSelected))
        var detailController = DetailViewController()
        self.navigationController?.pushViewController(detailController, animated: true)
    }

}

extension SearchCollectionViewController {
    func initSubscription(){
        subscriptions.append(GenreViewModel.subjectGroup.subscribe(onNext: { (groupFilms) in
            self.groupsFilms = groupFilms
            self.collectionView?.reloadData()
        }))
    }
}


extension UIImageView
{
    func addBlurEffect()
    {
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.bounds
        
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight] // for supporting device rotation
        self.addSubview(blurEffectView)
    }
}
