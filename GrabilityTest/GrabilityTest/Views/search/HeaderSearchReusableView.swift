//
//  HeaderSearchReusableView.swift
//  GrabilityTest
//
//  Created by Jorge Castro on 6/27/18.
//  Copyright © 2018 Jorge Castro. All rights reserved.
//

import UIKit

class HeaderSearchReusableView: UICollectionReusableView {

    @IBOutlet weak var sectionTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}
