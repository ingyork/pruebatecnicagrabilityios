//
//  SarchReducer.swift
//  GrabilityTest
//
//  Created by Jorge Castro on 7/4/18.
//  Copyright © 2018 Jorge Castro. All rights reserved.
//

import Foundation
import ReSwift

func navigationReducer(action: Action, state: NavigationState?) -> NavigationState {
    
    var state = state ?? NavigationState()
    
    switch action as? NavigationAction {
    case .hideNavigationBar?:
        state.navigationBarVisibility = false
    case .showNavigationBar?:
        state.navigationBarVisibility = true
    default:
        break
    }
    
    return state
}
