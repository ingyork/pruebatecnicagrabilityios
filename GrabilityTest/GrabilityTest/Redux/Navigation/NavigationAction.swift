//
//  SearchAction.swift
//  GrabilityTest
//
//  Created by Jorge Castro on 7/4/18.
//  Copyright © 2018 Jorge Castro. All rights reserved.
//

import ReSwift

enum NavigationAction: Action{
    case hideNavigationBar
    case showNavigationBar
}
