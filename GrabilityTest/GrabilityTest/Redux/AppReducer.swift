//
//  AppReducer.swift
//  GrabilityTest
//
//  Created by Jorge Castro on 7/4/18.
//  Copyright © 2018 Jorge Castro. All rights reserved.
//

import Foundation
import ReSwift

func appReducer(action: Action, state: AppState?) -> AppState {
    return AppState(
        navigationState: navigationReducer(action: action, state: state?.navigationState),
        detailState: detailReducer(action: action, state: state?.detailState)
    )
}
