//
//  AppState.swift
//  GrabilityTest
//
//  Created by Jorge Castro on 06/14/18.
//  Copyright © 2018 Jorge Castro. All rights reserved.
//

import ReSwift

struct AppState: StateType {
    var navigationState: NavigationState
    var detailState: DetailState
}
