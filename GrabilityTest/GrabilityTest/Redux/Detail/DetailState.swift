//
//  DetailState.swift
//  GrabilityTest
//
//  Created by Jorge Castro on 7/4/18.
//  Copyright © 2018 Jorge Castro. All rights reserved.
//

import Foundation
import ReSwift

struct DetailState: StateType {
    var film: FilmDTO?
}
