//
//  DetailAction.swift
//  GrabilityTest
//
//  Created by Jorge Castro on 7/4/18.
//  Copyright © 2018 Jorge Castro. All rights reserved.
//

import Foundation
import ReSwift

/*enum DetailAction: Action{
    case updateFilmDetail(film: FilmDTO)
}*/
/*struct UpdateFilmDetail: Action {
    let filmDetail: FilmDTO
}*/
class DetailAction {
    struct UpdateFilmDetail: Action {
        let filmDetail: FilmDTO
    }
}
