//
//  DetailReducer.swift
//  GrabilityTest
//
//  Created by Jorge Castro on 7/4/18.
//  Copyright © 2018 Jorge Castro. All rights reserved.
//

import Foundation
import ReSwift

func detailReducer(action: Action, state: DetailState?) -> DetailState {
    
    var state = state ?? DetailState(film: nil)
    /*switch action{
    case let action as UpdateFilmDetail:
        state.film = action.filmDetail
    default :
        break
    }*/
    
    switch action{
    case let action as DetailAction.UpdateFilmDetail:
        state.film = action.filmDetail
    default :
        break
    }
    
    return state
}
