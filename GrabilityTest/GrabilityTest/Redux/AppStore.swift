//
//  AppStore.swift
//  GrabilityTest
//
//  Created by Jorge Castro on 7/4/18.
//  Copyright © 2018 Jorge Castro. All rights reserved.
//

import ReSwift

class AppStore {
    
    func build() -> Store<AppState>{
        return Store<AppState>(reducer: appReducer, state: nil)
    }
}
