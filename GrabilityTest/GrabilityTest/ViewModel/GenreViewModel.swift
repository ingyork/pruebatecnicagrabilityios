//
//  GenreViewModel.swift
//  GrabilityTest
//
//  Created by Jorge Castro on 6/25/18.
//  Copyright © 2018 Jorge Castro. All rights reserved.
//

import RxSwift

class GenreViewModel {
    static func dispatchGenre(){
        ApiServices.getCategories()
    }
    
    static func dispatchGroupFilms(patter: String) {
        ApiServices.searchFilms(searchPatter: patter)
    }
}

extension GenreViewModel {
    static let subject = PublishSubject<[GenreDTO]?>()
    static let subjectGroup = PublishSubject<[GroupFilmGenreDTO]>()
}
