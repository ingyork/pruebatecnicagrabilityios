//
//  DetailViewModel.swift
//  GrabilityTest
//
//  Created by Jorge Castro on 06/22/18.
//  Copyright © 2018 Jorge Castro. All rights reserved.
//
import RxSwift

class DetailViewModel {
    
    static func dispatchDetailMovie(movieId: Int64){
        ApiServices.getMovieDetail(movieId: movieId)
    }
    
}


extension DetailViewModel {
    static let subject = PublishSubject<FilmDTO?>()
}
