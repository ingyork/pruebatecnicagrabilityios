//
//  MovieViewModel.swift
//  GrabilityTest
//
//  Created by Jorge Castro on 06/20/18.
//  Copyright © 2018 Jorge Castro. All rights reserved.
//

import Foundation
import RxSwift

class MoviewViewModel {
    
    static func dispatchListMoviesBy(categoryId: Int, category: String){
        ApiServices.getMoviesBy(categoryId: categoryId, category: category)
    }
    
}


extension MoviewViewModel {
    static let movieSubject = PublishSubject<[FilmDTO]?>()
}
