//
//  LifecycleAppllication.swift
//  GrabilityTest
//
//  Created by Jorge Castro on 06/22/18.
//  Copyright © 2018 Jorge Castro. All rights reserved.
//
import RxSwift

class RxUtils {
    static func unsubscribe(subscription: [Disposable]){
        subscription.forEach({ $0.dispose() })
    }
}
