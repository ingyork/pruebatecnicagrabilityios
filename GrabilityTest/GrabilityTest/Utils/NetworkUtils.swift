//
//  NetworkUtils.swift
//  GrabilityTest
//
//  Created by Jorge Castro on 6/25/18.
//  Copyright © 2018 Jorge Castro. All rights reserved.
//

import Alamofire

class NetworkUtils {
    class var isConnectedToInternet:Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}
