//
//  GroupFilmGenre.swift
//  GrabilityTest
//
//  Created by Jorge Castro on 06/27/18.
//  Copyright © 2018 Jorge Castro. All rights reserved.
//

import Foundation

class GroupFilmGenreDTO {
    
    var groupId: Int64 = 0
    var groupName: String = "Unknow"
    var films = [FilmDTO]()
}
