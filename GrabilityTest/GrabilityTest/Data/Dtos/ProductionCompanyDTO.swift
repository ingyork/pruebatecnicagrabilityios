//
//  ProductionCompany.swift
//  GrabilityTest
//
//  Created by Jorge Castro on 6/23/18.
//  Copyright © 2018 Jorge Castro. All rights reserved.
//

import ObjectMapper

class ProductionCompanyDTO: Mappable {
    
    var id: Int64?
    var name: String?
    
    init() {
        
    }
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
    }
    
    
}
