//
//  Film.swift
//  GrabilityTest
//
//  Created by Jorge Castro on 06/14/18.
//  Copyright © 2018 Jorge Castro. All rights reserved.
//

import ObjectMapper

class FilmDTO: Mappable {
    var voteCount: Int?
    var id: Int64?
    var vide: Bool?
    var voteAverage: Double?
    var title: String?
    var popularity: Double?
    var posterPath: String?
    var originalLanguage: String?
    var originalTitle: String?
    var backdropPath: String?
    var adult: Bool?
    var overview: String?
    var releaseDate: String?
    var homePage: String?
    var genres = [GenreDTO]()
    var genreIds = [Int]()
    var productionCompanies = [ProductionCompanyDTO]()
    
    init(){
        
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        voteCount <- map["vote_count"]
        id <- map["id"]
        vide <- map["video"]
        voteAverage <- map["vote_average"]
        title <- map["title"]
        popularity <- map["popularity"]
        posterPath <- map["poster_path"]
        originalLanguage <- map["original_language"]
        originalTitle <- map["original_title"]
        backdropPath <- map["backdrop_path"]
        adult <- map["adult"]
        overview <- map["overview"]
        releaseDate <- map["release_date"]
        homePage <- map["homepage"]
        genres <- map["genres"]
        genreIds <- map["genre_ids"]
        productionCompanies <- map["production_companies"]
    }
    
    
}
