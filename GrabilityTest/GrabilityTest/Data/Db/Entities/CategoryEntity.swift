//
//  CategoryEntity.swift
//  GrabilityTest
//
//  Created by Jorge Castro on 6/25/18.
//  Copyright © 2018 Jorge Castro. All rights reserved.
//

import RealmSwift

class CategoryEntity: Object {
    @objc dynamic var id: Int = 0
    @objc dynamic var rawId: String?
    @objc dynamic var name: String = ""
    @objc dynamic var imageName: String?
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
