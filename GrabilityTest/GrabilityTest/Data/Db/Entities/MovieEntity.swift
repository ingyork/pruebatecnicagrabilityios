//
//  MovieEntity.swift
//  GrabilityTest
//
//  Created by Jorge Castro on 6/25/18.
//  Copyright © 2018 Jorge Castro. All rights reserved.
//

import RealmSwift

class MovieEntity: Object {
    
    @objc dynamic var id: Int64 = 0
    @objc dynamic var categoryId: Int = 0
    @objc dynamic var title: String = ""
    @objc dynamic var voteCount: Int = 0
    @objc dynamic var video: String = ""
    @objc dynamic var voteAverage: Double = 0.0
    @objc dynamic var popularity: Double = 0.0
    @objc dynamic var posterPath: String = ""
    @objc dynamic var backdropPath: String = ""
    @objc dynamic var originalLanguage: String = ""
    @objc dynamic var originalTitle: String = ""
    @objc dynamic var overview: String = ""
    @objc dynamic var releaseDate: String = ""
    @objc dynamic var homePage: String = ""
    var genre = List<GenreEntity>()
    var productionCompany = List<ProductionCompanyEntity>()
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
