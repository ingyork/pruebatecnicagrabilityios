//
//  ProductionCompanyEntity.swift
//  GrabilityTest
//
//  Created by Jorge Castro on 6/26/18.
//  Copyright © 2018 Jorge Castro. All rights reserved.
//

import RealmSwift

class ProductionCompanyEntity: Object {
    
    @objc dynamic var id: Int64 = 0
    @objc dynamic var name: String = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
}
