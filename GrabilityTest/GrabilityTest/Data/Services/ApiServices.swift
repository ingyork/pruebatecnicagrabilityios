//
//  ApiServices.swift
//  GrabilityTest
//
//  Created by Jorge Castro on 06/20/18.
//  Copyright © 2018 Jorge Castro. All rights reserved.
//

import Foundation
import Alamofire
import RealmSwift

class CategoryKey {
    static let POPULAR = "popular"
    static let TOP_RATED = "top_rated"
    static let UPCOMING = "upcoming"
}

class ApiServices {
    
    static let USER_KEY = "51b6d3e433484ae65de2e3e73e92e939"
    static let BASE_URL = "https://api.themoviedb.org/3/movie/";
    static let BASE_URL_SEARCH = "https://api.themoviedb.org/3/search/movie";
    static let BASE_URL_GENRES = "https://api.themoviedb.org/3/genre/movie/";
    static let BASE_URL_IMAGE = "https://image.tmdb.org/t/p/w500"
    
    static func getMoviesBy(categoryId: Int, category: String){
        
        let dbHelper = DBHelper()
        if NetworkUtils.isConnectedToInternet {
            let URL = "\(BASE_URL)\(category)?api_key=\(USER_KEY)"
            Alamofire.request(URL).responseArray(keyPath: "results") { (response: DataResponse<[FilmDTO]>) in
                
                let responseDto = response.result.value
                MoviewViewModel.movieSubject.onNext(responseDto)
                var moviesEntities = [MovieEntity]()
                responseDto?.forEach({ (dto) in
                    let obj = MovieEntity()
                    obj.id = dto.id!
                    obj.categoryId = categoryId
                    obj.title = dto.title!
                    obj.voteCount = dto.voteCount!
                    obj.voteAverage = dto.voteAverage!
                    obj.overview = dto.overview!
                    obj.posterPath = dto.posterPath!
                    obj.homePage = (dto.homePage != nil) ? dto.homePage! : ""
                    
                    dto.genreIds.forEach({ (genreId) in
                        let gen: GenreEntity? = dbHelper.getRowBy(id: genreId)
                        guard let ge = gen else { return }
                        obj.genre.append(ge)
                    })
                    
                    moviesEntities.append(obj)
                })
                dbHelper.insertRows(moviesEntities)
            }
        }
        else if dbHelper.isNotEmptyTable(obj: MovieEntity.self) {
            
            let objs: Results<MovieEntity>? = dbHelper.getRowsByFilter(filter: "categoryId = \(categoryId)")
            var responseDto: [FilmDTO] = []
            
            objs?.forEach({ (res) in
                let m = FilmDTO()
                m.id = res.id
                m.title = res.title
                m.voteCount = res.voteCount
                m.voteAverage = res.voteAverage
                m.overview = res.overview
                m.posterPath = res.posterPath
                m.homePage = res.homePage
                responseDto.append(m)
            })
            MoviewViewModel.movieSubject.onNext(responseDto)
        }
        
    }
    
    static func getMovieDetail(movieId: Int64){
        
        let dbHelper = DBHelper()
        if NetworkUtils.isConnectedToInternet {
            
            let URL = "\(BASE_URL)\(movieId)?api_key=\(USER_KEY)"
            Alamofire.request(URL).responseObject(){ (response: DataResponse<FilmDTO>) in
                
                let res =  response.result.value
                let movie: MovieEntity? = dbHelper.getRowBy(id: movieId)
                var pcompanies = List<ProductionCompanyEntity>()
                
                res?.productionCompanies.forEach({ (p) in
                    if let id = p.id, let name = p.name {
                        let pc = ProductionCompanyEntity()
                        pc.id = id
                        pc.name = name
                        dbHelper.insertRow(pc)
                        pcompanies.append(pc)
                    }
                })
                
                movie?.productionCompany = pcompanies
                if let m = movie {
                    dbHelper.insertRow(m)
                }
                DetailViewModel.subject.onNext(res)
            }
        }
        else{
            if dbHelper.isNotEmptyTable(obj: MovieEntity.self) {
                let obj: MovieEntity? = dbHelper.getRowBy(id: Int(movieId))
                if let res = obj {
                    let m = FilmDTO()
                    m.id = res.id
                    m.title = res.title
                    m.voteCount = res.voteCount
                    m.voteAverage = res.voteAverage
                    m.overview = res.overview
                    m.posterPath = res.posterPath
                    m.homePage = res.homePage
                    
                    res.genre.forEach { (gr) in
                        let g = GenreDTO()
                        g.id = gr.id
                        g.name = gr.name
                        m.genres.append(g)
                    }
                    
                    res.productionCompany.forEach({ (p) in
                        let pr = ProductionCompanyDTO()
                        pr.id = p.id
                        pr.name = p.name
                        m.productionCompanies.append(pr)
                    })
                    
                    DetailViewModel.subject.onNext(m)
                }
            }
        }
    }
    
    static func getCategories(){
        let URL = "\(BASE_URL_GENRES)list?api_key=\(USER_KEY)"
        Alamofire.request(URL).responseArray(keyPath: "genres") { (response: DataResponse<[GenreDTO]>) in
            GenreViewModel.subject.onNext(response.result.value)
        }
    }
    
    static func searchFilms(searchPatter: String){
        let URL = "\(BASE_URL_SEARCH)?query=\(searchPatter)&api_key=\(USER_KEY)"
        Alamofire.request(URL).responseArray(keyPath: "results") { (response: DataResponse<[FilmDTO]>) in
            
            if let films = response.result.value {
                GenreViewModel.subjectGroup.onNext(self.getGroupFilmGenre(films: films))
            }
            else{
                GenreViewModel.subjectGroup.onNext([GroupFilmGenreDTO]())
            }
        }
    }
        
}

extension ApiServices {
    
    private static func getGroupFilmGenre(films: [FilmDTO]) -> [GroupFilmGenreDTO] {
        let genreIds = getGenreIdsFromRequest(films: films)
        let response = getGroupNameFilmGenre(genreIds: genreIds)
        response.forEach { (gf) in
            let filmGroupList = getFilmsByGroupId(films: films, genreId: gf.groupId)
            gf.films = filmGroupList
        }
        return response
    }
    
    private static func getGroupNameFilmGenre(genreIds: [Int]) -> [GroupFilmGenreDTO]{
        let dbHelper = DBHelper()
        var groupFilmGenres = [GroupFilmGenreDTO]()
        var stringIds = [Int]()
        genreIds.forEach { (genre) in
            stringIds.append(genre)
        }
        
        let genresDB: Results<GenreEntity>? = dbHelper.getRowsByFilterArray(filterArray: stringIds)
        
        if let genresDB = genresDB {
            for genre in genresDB {
                let group = GroupFilmGenreDTO()
                group.groupId = genre.id
                group.groupName = genre.name
                groupFilmGenres.append(group)
            }
        }
        
        return groupFilmGenres
    }
    
    
    private static func getFilmsByGroupId(films: [FilmDTO], genreId: Int64) -> [FilmDTO] {
        var filmGroupLis = [FilmDTO]()
        films.forEach { (film) in
            if !(film.genreIds.isEmpty) {
                film.genreIds.forEach({ (id) in
                    if id == genreId {
                        filmGroupLis.append(film)
                    }
                })
            }
        }
        
        return filmGroupLis
    }
    
    private static func getGenreIdsFromRequest(films: [FilmDTO]) -> [Int]{
        var genreIdsReturn = [Int]()
        films.forEach { (film) in
            if !film.genreIds.isEmpty {
                film.genreIds.forEach({ (genreId) in
                    if !genreIdsReturn.contains(genreId) {
                        genreIdsReturn.append(genreId)
                    }
                })
            }
        }
        return genreIdsReturn
    }
}
