//
//  AppDelegate.swift
//  GrabilityTest
//
//  Created by Jorge Castro on 6/10/18.
//  Copyright © 2018 Jorge Castro. All rights reserved.
//

import UIKit
import ReSwift
import RxSwift

let appStore = AppStore().build()

enum AppStateEvent: String {
    case willResignActive = "willResignActive"
    case willEnterBackground = "willEnterBackground"
    case willEnterForeground = "willEnterForeground"
}

let appDelegateObservable = PublishSubject<AppStateEvent>()

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, StoreSubscriber {
    
   typealias StoreSubscriberStateType = AppState
    
    var window: UIWindow?
    var uinavcontroller: UINavigationController?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        initWindow()
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        appDelegateObservable.onNext(.willEnterBackground)
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        appDelegateObservable.onNext(.willEnterForeground)
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
    }

    func applicationWillTerminate(_ application: UIApplication) {
    }
    
    func initWindow(){
        
        let uinav = UINavigationBar.appearance()
        uinav.tintColor = UIColor.red
        uinav.backgroundColor = UIColor.white
        
        
        window = UIWindow(frame: UIScreen.main.bounds)
        //window?.backgroundColor = UIColor.lightGray
        
        let flowLayout = UICollectionViewFlowLayout()
        //flowLayout.minimumLineSpacing = 0
        let categoryViewController = CategoryViewController(collectionViewLayout: flowLayout)
        
        uinavcontroller = UINavigationController(rootViewController: categoryViewController)
        window?.rootViewController = uinavcontroller
        
        appStore.subscribe(self)

        
        window?.makeKeyAndVisible()
    }

    
    func newState(state: AppState) {
        uinavcontroller?.isNavigationBarHidden = !state.navigationState.navigationBarVisibility
    }
    
}

